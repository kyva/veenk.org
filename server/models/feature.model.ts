import mongoose from 'mongoose'
const Schema = mongoose.Schema

const schema = new Schema(
	{
		name: Schema.Types.Mixed,
		description: Schema.Types.Mixed
	},
	{
		id: false,
		toJSON: { virtuals: true }
	}
)

export const FeatureModel = mongoose.model('Feature', schema)
