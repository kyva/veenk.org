import { getLangFromHeaders } from '~/server/utils/localization'
import projectService from '~/server/services/project.service'

export default defineEventHandler(async (event) => {
	const slug = event.context.params?.projectSlug

	if (!slug) {
		throw createError({
			statusCode: 400,
			message: 'Bad request, slug is required to fetch project!'
		})
	}

	const filters = {
		slug: slug,
		isActive: true,
		isReady: true
	}
	const lang = getLangFromHeaders(event.headers)

	try {
		const project = await projectService.getProjectProfile({ filters, lang })
		if (!project) throw new Error()
		return project
	} catch (err) {
		console.error('Error: ', err)
		throw createError({
			statusCode: 404,
			statusMessage: 'Project profile not found!'
		})
	}
})
