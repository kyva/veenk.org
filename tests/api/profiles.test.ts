import { describe, it, expect } from 'vitest'
import { API_URL } from './setup'

describe('Profiles endpoints', async () => {
	it.todo('/profiles/all endpoint status 200', async () => {
		const res = await fetch(`${API_URL}/profiles/all`)
		expect(res.status).toEqual(200)
	})
})
