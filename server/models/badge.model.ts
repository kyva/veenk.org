import mongoose from 'mongoose'
const Schema = mongoose.Schema

const schema = new Schema(
	{
		key: {
			type: String,
			required: true,
			unique: true
		}
	},
	{
		id: false,
		toJSON: { virtuals: true }
	}
)

export const BadgeModel = mongoose.model('badges', schema)
