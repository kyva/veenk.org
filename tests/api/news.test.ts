import { describe, it, expect } from 'vitest'
import { API_URL } from './setup'

describe('News endpoint', async () => {
	it('/news/all endpoint status 200', async () => {
		const res = await fetch(`${API_URL}/news/all`)
		expect(res.status).toEqual(200)
	})
})
