interface IFetchOptions {
	method: 'GET' | 'POST'
}

export const useCustomFetch = () => {
	const { $i18n } = useNuxtApp()

	const fetchOptions = {
		baseURL: '/api/',
		headers: {
			ContentType: 'application/json',
			'Accept-Language': $i18n.locale.value,
			'Access-Control-Allow-Origin': '*'
		}
	}

	const fetch = <T>(url: string, options: IFetchOptions) => {
		return $fetch<T>(url, { ...fetchOptions, ...options })
	}

	const getReq = <T>(url: string) => {
		return fetch<T>(url, { method: 'GET' })
	}

	return { fetch, getReq }
}
