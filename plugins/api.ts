import ProjectsModule from '~/services/fetch/modules/projects'
import RoadmapModule from '~/services/fetch/modules/roadmap'
import InternalFinancesModule from '~/services/fetch/modules/internalFinances'

// export default defineNuxtPlugin((nuxtApp) => {
// 	const api = $fetch.create({
// 		baseURL: useRuntimeConfig().public.apiUrl ?? 'https://api.veenk.org',
// 		headers: {
// 			ContentType: 'application/json',
// 			SecureVeenkOrigin: 'true',
// 		},
// 	})

// 	const modules: ApiInstance = {
// 		projects: projectsModule(apiFecher),
// 	}

// 	return {
// 		provide: {
// 			api: modules,
// 		},
// 	}
// })

export default defineNuxtPlugin(() => {
	const modules = {
		projects: ProjectsModule(),
		roadmap: RoadmapModule(),
		internalFinances: InternalFinancesModule(),
	}
	return {
		provide: {
			api: modules,
		},
	}
})
