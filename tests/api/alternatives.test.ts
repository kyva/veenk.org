import { describe, it, expect } from 'vitest'
import { API_URL } from './setup'

describe('Alternatives endpoint', async () => {
	it('/alternatives/all endpoint status 200', async () => {
		const res = await fetch(`${API_URL}/alternatives/all`)
		expect(res.status).toEqual(200)
	})
})
