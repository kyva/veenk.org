import type { Component } from 'vue'
import Android from './Android.vue'
import Selfhost from './Selfhost.vue'
import Web from './Web.vue'
import Ios from './Ios.vue'
import Windows from './Windows.vue'
import Mac from './Mac.vue'
import Linux from './Linux.vue'
import Dev from './Dev.vue'

const icons: { [key in PlatformsWithIcon]: Component } = {
	android: Android,
	linux: Linux,
	mac: Mac,
	windows: Windows,
	ios: Ios,
	web: Web,
	'self-host': Selfhost,
	dev: Dev,
}

export default icons
