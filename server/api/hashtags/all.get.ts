import hashtagService from '~/server/services/hashtags.service'
import { debugError } from '#shared/utils/debug'

export default defineEventHandler(async (event) => {
	const lang = getLangFromHeaders(event.headers)
	try {
		const items = await hashtagService.getAll({ lang })
		if (items.length === 0) throw new Error()
		return items
	} catch (err: unknown) {
		debugError(err)
		throw createError({
			statusCode: 500,
			message: 'Server error while fetching hashtags!'
		})
	}
})
