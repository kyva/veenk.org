const ProjectsModule = () => {
	const { getReq } = useCustomFetch()
	const MODULE_URL = '/finances'

	const getStats = () => getReq<FinancesStats>(`${MODULE_URL}/stats`)
	const getList = () => getReq<InternalFinances[]>(`${MODULE_URL}/internal-list`)

	return {
		getStats,
		getList
	}
}

export default ProjectsModule
