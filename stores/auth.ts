import { defineStore } from 'pinia'

export const useAuthStore = defineStore('auth', () => {
	const user = ref<User>()
	const token = useCookie('MY_COOKIE', {
		maxAge: 60 * 60
	})

	const setToken = (data?: string) => {
		token.value = data
	}
	const setUser = (data?: User) => {
		user.value = data
	}

	const fetchUser = async () => {
		if (!token.value) return
		try {
			const res = await $fetch<User>('/api/auth/user')

			// setToken(res.token)

			setUser(res)
		} catch (error) {
			setToken()
			setUser()
			console.error(error)
		}
	}

	const signIn = async (data: Login) => {
		try {
			const res = await $fetch<User>('/api/auth/login', {
				method: 'POST',
				body: data
			})

			// setToken(res.token)

			setUser(res)
		} catch (error) {
			setToken()
			setUser()
			console.error(error)
		}
	}

	const logout = () => {
		setToken()
		setUser()
	}

	return {
		user,
		token,
		signIn,
		logout,
		fetchUser
	}
})
