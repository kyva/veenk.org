import { describe, it, expect } from 'vitest'
import { API_URL } from './setup'
import { PER_PAGE } from '~/constants/projects'

describe('Projects endpoints', async () => {
	it('/projects/all endpoint status 200', async () => {
		const res = await fetch(`${API_URL}/projects/all`)
		expect(res.status).toEqual(200)
	})
	it('/projects/[projectSlug]/profile endpoint status 200', async () => {
		const projectSlug = 'krita'
		const res = await fetch(`${API_URL}/projects/${projectSlug}/profile`)
		expect(res.status).toEqual(200)
	})
	it('/projects/[projectSlug]/donation-info endpoint status 200', async () => {
		const projectSlug = 'krita'
		const res = await fetch(`${API_URL}/projects/${projectSlug}/donation-info`)
		expect(res.status).toEqual(200)
	})
	it('/projects/list endpoint status 200', async () => {
		const res = await fetch(`${API_URL}/projects/list`)
		expect(res.status).toEqual(200)
	})
})

describe('Project Profile', async () => {
	it('/projects/[projectSlug]/profile return project', async () => {
		const projectSlug = 'krita'
		const res = await fetch(`${API_URL}/projects/${projectSlug}/profile`)
		const body: ProjectProfile = await res.json()
		expect(body.slug).toEqual(projectSlug)
	})
})

describe('Project Donation Info', async () => {
	it('/projects/[projectSlug]/profile return project donation info', async () => {
		const projectSlug = 'krita'
		const kritaCreated = '2020-10-14'
		const res = await fetch(`${API_URL}/projects/${projectSlug}/donation-info`)
		const body: ProjectDonationInfo = await res.json()
		expect(body.created).toEqual(kritaCreated)
	})
})

describe('Projects listed', async () => {
	it('/projects/list endpoint status 200', async () => {
		const res = await fetch(`${API_URL}/projects/list`)
		const body: ProjectList = await res.json()
		expect(body.length).toEqual(PER_PAGE)
	})
})
