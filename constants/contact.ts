export const socialIcons: string[] = [
	'twitter',
	'reddit',
	'instagram',
	'linkedin',
	'facebook',
	'youtube',
	'mastodon',
	'deviantart',
	'vk',
	'pixelfed'
]

export const downloadTypes: { [key: string]: { name: string; icon: string } } = {
	appleStore: {
		name: 'Apple Store',
		icon: 'ph:app-store-logo-fill'
	},
	playStore: {
		name: 'Play Store',
		icon: 'ph:google-play-logo-fill'
	},
	windows: {
		name: 'Windows',
		icon: 'ph:windows-logo-fill'
	},
	linux: {
		name: 'Linux',
		icon: 'ph:linux-logo-fill'
	},
	snapStore: {
		name: 'Snap Store',
		icon: 'ph:discord-logo'
	},
	fdroid: {
		name: 'F-Droid',
		icon: 'ph:discord-logo'
	}
}

export const CONTACT_LINKS = ['forum', 'matrix', 'xmpp', 'irc', 'slack', 'telegram', 'discord', 'zulip'] as const

declare global {
	type ContactType = (typeof CONTACT_LINKS)[number]

	type ContactLinkIcon = {
		[key in ContactType]: {
			icon: string
		}
	}
}

export const iconType: ContactLinkIcon = {
	forum: {
		icon: 'ph:lifebuoy'
	},
	matrix: {
		icon: 'ph:chats-teardrop'
	},
	xmpp: {
		icon: 'ph:chats-teardrop'
	},
	irc: {
		icon: 'ph:chats-teardrop'
	},
	slack: {
		icon: 'ph:slack-logo'
	},
	telegram: {
		icon: 'ph:telegram-logo'
	},
	discord: {
		icon: 'ph:discord-logo'
	},
	zulip: {
		icon: 'ph:chats-teardrop'
	}
}
