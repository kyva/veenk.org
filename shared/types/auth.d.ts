interface User {
	id: number
	email: string
	token: string
	profile: Pofile
}

interface Profile {
	id: number
	username: string
	email: string
	token: string
	avatar: string
	patronOf?: string[]
	teamOf?: string[]
}

interface Login {
	username: string
	password: string
}
