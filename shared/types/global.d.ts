/**
 *
 * Define all global types in this file.
 *
 */

type KeysWithId<T> = keyof T | '_id'

type MongoSelect<T> = { [key in KeysWithId<T>]: number }

type PlatformsWithIcon = 'android' | 'linux' | 'mac' | 'windows' | 'ios' | 'web' | 'self-host' | 'dev'

type Views = 'user' | 'funders' | 'devs' | 'translators'
type DonationOptions =
	| 'liberapay'
	| 'opencollective'
	| 'patreon'
	| 'veenk'
	| 'github-sponsors'
	| 'paypal'
	| 'buymeacoffee'
	| 'ko-fi'
	| 'custom'

type IconComponent = DonationOptions

type SocialPlatforms =
	| 'twitter'
	| 'reddit'
	| 'instagram'
	| 'linkedin'
	| 'facebook'
	| 'youtube'
	| 'mastodon'
	| 'deviantart'
	| 'vk'
	| 'pixelfed'

type DownloadType = 'appleStore' | 'windows' | 'linux' | 'snapStore' | 'playStore' | 'fdroid'
type LocalizationSoftwareType = 'weblate' | 'crowdin'
type NewsType = 'version' | 'event' | 'launch' | 'goal'
type InstanceType = 'peeko' | 'meetiko' | 'luzeed' | 'veedeo'

interface Image {
	src: string
	alt: string
}

type LocalizedText = {
	[key in LocaleKey]?: string
}

type Site = {
	website: string
	author: string
	desc: string
	title: string
	ogImage?: string
	lightAndDarkMode: boolean
	postPerPage: number
}

interface New {
	color: string
	type: NewsType
	text: string
}

interface Instance {
	type: InstanceType
	software: string
	softwareUrl: string
	url: string
	userCount?: number
	lang?: string
	statusUrl?: string
}

interface PlatformAPI {
	type: PlatformTypeRaw
	name: string
}

interface ImportHelper {
	repositories: {
		platformType: PlatformTypeAPI
		repoType: RepositoryType
		repoName: string
	}[]
}
interface ExternalData {
	projectId: string
	history: ImportedRepoData[]
	lastRepoImport: ImportedRepoData
}

type ImportedRepo = {
	[key in PlatformType]: {
		stars: number
		forks: number
		lastRelease: {
			name: string
			publish_at: string
		}
	}
}

interface ImportedRepoData {
	date: string
	repos: ImportedRepo
}

interface Feature {
	name: string
	description: string
}

interface Sponsor {
	name: string
	url: string
}

interface DownloadLink {
	url: string
	type: DownloadType
}

interface ProjectGoal {
	name: string
	amount: number
	collected: number
	finish: string // date UTC
	type: 'monthly' | 'fixed'
}

interface Organization {
	name: string
	url: string
}

interface SocialLink {
	type: SocialPlatforms
	url: string
}
interface ContactLink {
	type: ContactType
	url: string
}

interface Links {
	type: string
	url: string
}

interface LocalizationSoftware {
	type: LocalizationSoftwareType
	badge: string
	url: string
}

interface ProjectPlatform {
	type: PlatformType
	platformsList?: PlatformTypeRaw[]
	license: string
	repositories: Repository[]
	codingLangs: CodingLangType[]
}

interface ProjectMinimal {
	slug: string
	name: string
	logoUrl: string
}

type ProjectMinimalWithReady = ProjectMinimal & {
	isReady: boolean
}

type ProjectDB = ProjectMinimal & {
	_id: string
	isActive: boolean
	isReady: boolean
	location: string
	karma: number
	created: string
	importHelper: ImportHelper
	webUrl: string
	fullDescription: string
	audited: boolean
	features: Feature[]
	downloadLinks: DownloadLink[]
	team: Profile[]
	patrons: Profile[]
	socialLinks: SocialLink[]
	contactLinks: ContactLink[]
	description: LocalizedText
	fromOrganization: boolean
	organization: Organization
	members: number
	tags: Hashtag[]
	badges: BadgeType[]
	platforms: ProjectPlatform[]
	alternativeTo: Alternative[]
	screenshots: Image[]
	finances: FinanceInfo
	languages: LocaleKey[]
	externalData: ExternalData
	localizationSoftware: LocalizationSoftware
}

type ProjectDonationInfoFields = 'webUrl' | 'created' | 'members' | 'team' | 'finances' | 'fromOrganization'

type ProjectCardFields =
	| 'isReady'
	| 'description'
	| 'fromOrganization'
	| 'organization'
	| 'members'
	| 'tags'
	| 'badges'
	| 'platforms'
	| 'alternativeTo'
	| 'screenshots'
	| 'finances'
	| 'languages'
	| 'externalData'
	| 'localizationSoftware'

type ProjectProfileFields =
	| 'description'
	| 'fromOrganization'
	| 'organization'
	| 'members'
	| 'tags'
	| 'badges'
	| 'platforms'
	| 'alternativeTo'
	| 'screenshots'
	| 'finances'
	| 'languages'
	| 'externalData'
	| 'localizationSoftware'
	| 'webUrl'
	| 'fullDescription'
	| 'audited'
	| 'features'
	| 'downloadLinks'
	| 'team'
	| 'patrons'
	| 'socialLinks'
	| 'contactLinks'

type ProjectDonationInfo = ProjectMinimal & Pick<ProjectDB, ProjectDonationInfoFields>
type ProjectList = Array<ProjectCard | ProjectMinimalWithReady>
type ProjectCard = ProjectMinimal & Pick<ProjectDB, ProjectCardFields>
type ProjectProfile = ProjectMinimal & Pick<ProjectDB, ProjectProfileFields>

interface Repository {
	type: RepositoryType
	name: string
	url: string
	stars: number
	forks: number
}
interface FinanceInfo {
	actualName: string
	actualCollected: number
	actualGoal: number
	totalCollected: number
	totalGoal: number
	goal: ProjectGoal
	donationOptions: DonationOption[]
	customDonation: CustomDonation
	sponsors: Sponsor[]
}
interface DonationOption {
	type: DonationOptions
	url: string
	monthlyCollected: number
	totalCollected: number
}

interface CustomDonation {
	url: string
	paypal: boolean
	stripe: boolean
	mollie: boolean
	crypto: boolean
	bankTransfer: boolean
	monetico: boolean
	donorbox: boolean
	hubspot: boolean
	clickandpledge: boolean
}

interface Alternative {
	[key: string]: string | number
	slug: string
	name: string
	img: string
	count: number
}

interface Alternatives {
	[key: string]: Alternative
}

interface Hashtag {
	[key: string]: string | number | string[]
	_id: string
	key: string
	count: number
	related: string[]
	name: string
	label: string
}

interface Hashtags {
	[key: string]: Hashtag
}

interface VeenkStats {
	actualCollected: number | null
	actualGoal: number | null
	totalCollected: number
	totalGoal: number
}

interface FinancesStats {
	month: number
	year: number
	actualCollected: number | null
	actualGoal: number | null
	totalCollected: number
	totalGoal: number
}

interface FinanceRecord {
	[key: string]: string | number
	date: string
	name: string
	amount: number
	source: string
	sourceName: string
}

interface InternalFinances {
	name: string
	month: number
	year: number
	shortName: string
	total_expended: number
	total_collected: number
	expenses: FinanceRecord[]
	takings: FinanceRecord[]
}

interface Roadmap {
	isActive: boolean
	date: string
	dateDisplay: string
	name: LocalizedText
	description: LocalizedText
	comingSoon: boolean
	future: boolean
}

type CarrouselType = 'closeToGoal' | 'successfullyFunded'

// interface HTMLDivElement {
// 	offsetWidth?: number
// }
