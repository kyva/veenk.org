import type { SortOrder } from 'mongoose'
import { InternalFinancesModel } from '../models/finances.model'
import { financialUtils } from '../utils/financialUtils'

const getInternalStats = async () => {
	const today = new Date()

	const currentMonth = today.getMonth() + 1
	const currentYear = today.getFullYear()

	const monthlyStats = await InternalFinancesModel.findOne({
		month: currentMonth,
		year: currentYear
	})

	let monthlyExpended: number | null = null
	let monthlyCollected: number | null = null

	if (monthlyStats) {
		monthlyExpended = Math.ceil(financialUtils.calculateTotal(monthlyStats.expenses))
		monthlyCollected = Math.ceil(financialUtils.calculateTotal(monthlyStats.takings))
	}

	const finances = await InternalFinancesModel.find({
		$or: [{ year: { $lt: currentYear } }, { year: { $eq: currentYear }, month: { $lte: currentMonth } }]
	})
	const stats = financialUtils.calculateAllTime(finances)
	return {
		month: currentMonth,
		year: currentYear,
		actualCollected: monthlyCollected,
		actualGoal: monthlyExpended,
		totalCollected: Math.ceil(stats.takings),
		totalGoal: Math.ceil(stats.expenses)
	}
}

const getInternalFinances = async () => {
	const today = new Date()

	const currentMonth = today.getMonth() + 1
	const currentYear = today.getFullYear()

	const sort_query: { [key: string]: SortOrder } = { year: -1, month: -1 }
	const months = await InternalFinancesModel.find({
		$or: [{ year: { $lt: currentYear } }, { year: { $eq: currentYear }, month: { $lte: currentMonth } }]
	}).sort(sort_query)

	const monthsWithStats = months.map((month) => {
		return {
			month: month.month,
			year: month.year,
			total_expended: Math.ceil(financialUtils.calculateTotal(month.expenses)),
			total_collected: Math.ceil(financialUtils.calculateTotal(month.takings)),
			expenses: financialUtils.formatInvoices(month.expenses),
			takings: financialUtils.formatInvoices(month.takings)
		}
	})

	return monthsWithStats
}

const addFromObj = (data: unknown) => InternalFinancesModel.insertMany(data)

export default {
	getInternalStats,
	getInternalFinances,
	addFromObj
}
