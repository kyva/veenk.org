import financesService from '~/server/services/finances.service'

export default defineEventHandler(async () => {
	try {
		const items = await financesService.getInternalFinances()
		return items
	} catch (err) {
		console.error('Error: ', err)
		throw createError({
			statusCode: 500,
			statusMessage: 'Server error when fetching internal finances list!'
		})
	}
})
