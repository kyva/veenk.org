import projectService from '~/server/services/project.service'
import { debugError } from '#shared/utils/debug'

export default defineEventHandler(async () => {
	const filters = {
		isActive: true,
		isReady: true
	}

	try {
		const projects = await projectService.getAll({ filters })
		if (projects.length === 0) throw new Error()
		return projects
	} catch (err) {
		debugError(err)
		throw createError({
			statusCode: 500,
			message: 'Server error while fetching all Projects!'
		})
	}
})
