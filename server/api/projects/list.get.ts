import { PER_PAGE } from '~/constants/projects'
import projectService from '~/server/services/project.service'
import { debugError } from '~/shared/utils/debug'

export default defineEventHandler(async (event) => {
	const query = getQuery(event)

	const page = query.page ? +query.page : 0

	const filters: ProjectsListFilters = {
		isActive: true
	}

	const options: ProjectListOptions = {
		limit: PER_PAGE,
		skip: page * PER_PAGE
	}

	if (typeof query.hasBadge === 'string') {
		filters.badges = { $in: query.hasBadge.split(' ') }
	}

	if (typeof query.hasPlatform === 'string') {
		filters.platforms = {
			$elemMatch: { type: { $in: query.hasPlatform.split(' ') } }
		}
	}

	const pageFilter: ProjectPageFilter = {
		alternativeTo: null,
		hashtag: null
	}

	if (typeof query.alternative === 'string') {
		pageFilter.alternativeTo = query.alternative
	}
	if (typeof query.hashtag === 'string') {
		pageFilter.hashtag = query.hashtag
	}

	const lang = getLangFromHeaders(event.headers)

	try {
		const projects = projectService.getListed({ filters, options, pageFilter, lang })
		return projects
	} catch (err) {
		debugError(err)
		throw createError({
			statusCode: 500,
			message: `Could not fetch listed projects. Page: ${page}!`
		})
	}
})
