export default defineNuxtPlugin((nuxtApp) => {
	return {
		provide: {
			scrollTop: () => {
				window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
			},
		},
	}
})
