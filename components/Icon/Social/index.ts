import type { Component } from 'vue'
import Twitter from './Twitter.vue'
import Reddit from './Reddit.vue'
import Instagram from './Instagram.vue'
import LinkedIn from './LinkedIn.vue'
import Facebook from './Facebook.vue'
import Youtube from './Youtube.vue'
import Mastodon from './Mastodon.vue'
import DevianArt from './DevianArt.vue'
import VK from './VK.vue'
import Pixelfed from './Pixelfed.vue'

const icons: { [key in SocialPlatforms]: Component } = {
	twitter: Twitter,
	reddit: Reddit,
	instagram: Instagram,
	linkedin: LinkedIn,
	facebook: Facebook,
	youtube: Youtube,
	mastodon: Mastodon,
	deviantart: DevianArt,
	vk: VK,
	pixelfed: Pixelfed,
}

export default icons
