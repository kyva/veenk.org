import { langs } from '~/constants/langs'

export const getLocalizedText = (text: LocalizedText): string => {
	const { locale } = useI18n()
	const lang = locale.value as LocaleKey
	return text[lang] ? text[lang] : (text.en ?? 'Localization Error')
}

export const getKeyFromName = (name: LocaleName): LocaleKey =>
	Object.values(langs).filter((lang) => lang.name === name)[0].key

export const getLangName = (key: LocaleKey): LocaleName => langs[key]?.name ?? `no-${key}`

export const getTranlsationFirst = (text: string, key = '{name}') => {
	const [first, _] = text.split(key)
	return first
}
export const getTranlsationLast = (text: string, key = '{name}') => {
	const [_, last] = text.split(key)
	return last
}
