import { NewsModel } from '../models/news.model'

export const newsService = {
	getAll
}

async function getAll(filters = {}) {
	return await NewsModel.find(filters)
}
