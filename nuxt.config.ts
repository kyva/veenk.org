// https://nuxt.com/docs/api/configuration/nuxt-config
import locales from './locales/locales'

export default defineNuxtConfig({
	compatibilityDate: '2024-08-03',
	devtools: { enabled: false },
	app: {
		head: {
			link: [{ rel: 'icon', type: 'image/svg+xml', href: '/favicon.svg' }]
		}
	},
	modules: [
		'@nuxt/test-utils/module',
		'@nuxtjs/i18n',
		'@nuxt/icon',
		'@nuxt/eslint',
		'@pinia/nuxt',
		'@nuxtjs/color-mode',
		'@nuxt/image',
		'@vueuse/nuxt'
	],
	components: {
		dirs: ['./components', './managers']
	},
	i18n: {
		vueI18n: './locales/i18n.config.ts', // if you are using custom path, default
		locales,
		defaultLocale: 'en',
		strategy: 'prefix_except_default'
	},
	pinia: {
		storesDirs: ['./stores/**']
	},
	colorMode: {
		preference: 'system', // default value of $colorMode.preference
		fallback: 'dark', // fallback value if not system preference found
		hid: 'nuxt-color-mode-script',
		globalName: '__NUXT_COLOR_MODE__',
		classPrefix: '',
		classSuffix: '-mode',
		storageKey: 'nuxt-color-mode'
	},
	vite: {
		css: {
			preprocessorOptions: {
				sass: {
					api: 'modern-compiler',
					additionalData: '@use "~/assets/styles/sass/global.sass" as *'
				}
			}
		}
	},
	runtimeConfig: {
		authSecret: process.env.AUTH_SECRET,
		mongodbUri: process.env.MONGODB_URI
	},
	routeRules: {
		'/dashboard/**': { ssr: false }
		// '/projects/**': { ssr: false }
	},
	image: {
		domains: ['ams1.vultrobjects.com']
	},
	typescript: {
		typeCheck: true,
		strict: true
	}
})
