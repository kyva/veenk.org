import icons from '~/components/Icon/Repo'

declare global {
	type RepositoryType = (typeof REPOSITORY_TYPE)[number]

	type RepositoryList = {
		[key in RepositoryType]: {
			name: string
		}
	}
}

export const REPOSITORY_TYPE = [
	'github',
	'gitlab',
	'gitlab-selfhost',
	'gitea',
	'codeberg',
	'forgejo',
	'sourceforge',
	'gerrit',
	'custom'
] as const

export const repositories: RepositoryList = {
	github: {
		name: 'Github'
	},
	gitlab: {
		name: 'GitLab'
	},
	'gitlab-selfhost': {
		name: 'GitLab selfhost'
	},
	gitea: {
		name: 'Gitea'
	},
	codeberg: {
		name: 'Codeberg'
	},
	forgejo: {
		name: 'Forgejo'
	},
	sourceforge: {
		name: 'SourceForge'
	},
	gerrit: {
		name: 'Gerrit'
	},
	custom: {
		name: 'Custom'
	}
}
