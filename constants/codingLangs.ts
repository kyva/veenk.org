declare global {
	type CodingLangType = (typeof CODING_LANGS)[number]

	type CodingLangs = { [key in CodingLangType]: string }
}

const CODING_LANGS = [
	'astro',
	'css',
	'c',
	'c#',
	'c++',
	'elixir',
	'go',
	'js',
	'lua',
	'php',
	'plpgsql',
	'py',
	'roff',
	'ruby',
	'rust',
	'ts',
	'twig',
] as const

export const codingLangs: CodingLangs = {
	astro: 'Astro',
	css: 'CSS',
	c: 'C',
	'c++': 'C++',
	'c#': 'C#',
	elixir: 'Elixir',
	go: 'Go',
	js: 'Javascript',
	lua: 'Lua',
	php: 'PHP',
	plpgsql: 'PLpgSQL',
	py: 'Python',
	roff: 'Roff',
	ruby: 'Ruby',
	rust: 'Rust',
	ts: 'Typescript',
	twig: 'Twig'
}
