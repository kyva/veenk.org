// biome-ignore lint/suspicious/noExplicitAny: <object>
export const toKeyObject: (array: any[], key: string) => any = (array, key) => {
	// biome-ignore lint/suspicious/noExplicitAny: <object>
	const formated: any = {}
	for (const item of array) {
		formated[item[key]] = item
	}
	return formated
}

export const deepEqual = <T>(obj1: T, obj2: T) => JSON.stringify(obj1) === JSON.stringify(obj2)
