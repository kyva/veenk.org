export default defineNuxtRouteMiddleware((to) => {
	const authStore = useAuthStore()

	if (to.path === '/login') return

	console.log('authsTreuser: ', authStore.user)
	if (authStore.user) {
		return
	}

	return navigateTo(`/login?redirectTo=${to.path}`)
})
