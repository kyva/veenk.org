import mongoose from 'mongoose'
const Schema = mongoose.Schema

const TransactionType = {
	date: Date,
	name: Schema.Types.Mixed,
	amount: Number,
	sourceName: String,
	source: String
}

const internalFinancesSchema = new Schema<InternalFinances>(
	{
		name: {
			type: String,
			required: true,
			unique: true
		},
		shortName: String,
		expenses: [TransactionType],
		takings: [TransactionType],
		month: Number,
		year: Number
	},
	{
		id: false,
		toJSON: { virtuals: true },
		collection: 'internal_finances',
		timestamps: true
	}
)

export const InternalFinancesModel = mongoose.model('InternalFinances', internalFinancesSchema)
