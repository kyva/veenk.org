export const newsIcons: {
	[key: string]: { icon: string; color: string }
} = {
	version: {
		icon: 'ph:lightning',
		color: 'green',
	},
	event: {
		icon: 'ph:calendar-blank',
		color: 'red',
	},
	launch: {
		icon: 'ph:rocket-launch',
		color: 'yellow',
	},
	goal: {
		icon: 'ph:hand-heart',
		color: 'red',
	},
}
