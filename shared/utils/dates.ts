import moment from 'moment'
import 'moment/dist/locale/es'

export const getTimeFromNow = (date: string) => {
	const { locale } = useI18n()
	moment.locale(locale.value)
	return moment(date).fromNow()
}

export const timeWorkingOnProject = (str: string | undefined): number => {
	if (!str) throw new Error('String missing.')
	const created = Number(new Date(str).getFullYear())
	if (!created) throw new Error('Bad date format.')
	const today = new Date()
	const currentYear = +today.getFullYear()
	if (currentYear === created) {
		return 0
	}
	const yearsWorking = currentYear - created
	return yearsWorking
}
