import { debugError } from '~/shared/utils/debug'

export const useProjects = () => {
	const { $api } = useNuxtApp()
	const loading = ref(false)
	const projectsStore = useProjectsStore()

	const fetchAllProjects = async () => {
		if (projectsStore.allProjects.length > 0) return
		loading.value = true
		// const data = await $api.projects.getAllProjects()
		// loading.value = false
		// projectsStore.setAllProjects(data)

		try {
			const data = await $api.projects.getAllProjects()
			projectsStore.setAllProjects(data)
		} catch (err) {
			debugError(err)
		}
		loading.value = false
	}

	const checkSlug = async (slug: string) => {
		await fetchAllProjects()

		const project = projectsStore.getProjectBySlug(slug)

		return !!project
	}

	const getDonationInfoBySlug = async (slug: string) => {
		let project = projectsStore.getProjectDonationInfo(slug)
		if (project) return project
		project = await $api.projects.getProjectDonationInfoBySlug(slug)

		projectsStore.addProjectToDonationInfo(project)

		return project
	}

	return {
		checkSlug,
		loading,
		fetchAllProjects,
		getDonationInfoBySlug
	}
}
