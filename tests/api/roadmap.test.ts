import { describe, it, expect } from 'vitest'
// import { setup } from '@nuxt/test-utils'

describe('Roadmap endpoint', async () => {
	// await setup({
	// 	server: true
	// })

	it('/roadmap/app endpoint status 200', async () => {
		const res = await fetch('http://localhost:3000/api/roadmap/all')
		expect(res.status).toEqual(200)
	})
})
