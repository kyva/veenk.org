interface ProjectParams {
	alternative?: string
	page: number
	hashtag?: string
	platforms: PlatformTypeRaw[]
	badges: BadgeType[]
}

interface ProjectFilters {
	activePlatforms?: PlatformTypeRaw[]
	activeBadges?: BadgeType[]
}

// API

interface ProjectFiltersAPI {
	isActive: boolean
	isReady: boolean
}

type ProjectFiltersBySlug = ProjectFiltersAPI & {
	slug: string
}

interface ProjectsListFilters {
	isActive: boolean
	badges?: MongooseBaseQueryOptions
	tags?: MongooseBaseQueryOptions
	platforms?: MongooseBaseQueryOptions
	alternativeTo?: Types.ObjectId
}

interface ProjectPageFilter {
	alternativeTo: string | null
	hashtag: string | null
}

interface ProjectListOptions {
	limit: number
	skip: number
}

type RepoData = {
	[key: RepoData | string]: {
		name?: string
		publish_at?: string
		stars?: number
		forks?: number
		lastRelease: FormatData
	}
}
