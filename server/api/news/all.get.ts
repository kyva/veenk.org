import { newsService } from '~/server/services/news.service'
import { debugError } from '#shared/utils/debug'
export default defineEventHandler(async () => {
	const today = new Date()
	const filters = {
		startTime: {
			$lte: today
		},
		endTime: {
			$gte: today
		}
	}

	try {
		const items = await newsService.getAll(filters)
		return items
	} catch (err) {
		debugError(err)
		throw createError({
			statusCode: 500,
			message: 'Server error when fetching news!'
		})
	}
})
