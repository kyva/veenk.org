import { describe, it, expect } from 'vitest'
import { API_URL } from './setup'

const setLangHeader = (lang: string) => {
	return {
		headers: {
			'accept-language': lang
		}
	}
}

describe('Hashtags endpoint', async () => {
	it('/hashtags/all endpoint status 200', async () => {
		const res = await fetch(`${API_URL}/hashtags/all`)
		expect(res.status).toEqual(200)
	})
	it('/hashtags/all endpoint status 200', async () => {
		const hahtagKey = 'fediverse'
		const res = await fetch(`${API_URL}/hashtags/${hahtagKey}`)
		expect(res.status).toEqual(200)
	})
	it('/hashtags/all endpoint return hashtag', async () => {
		const hahtagKey = 'fediverse'
		const res = await fetch(`${API_URL}/hashtags/${hahtagKey}`)
		const body: Hashtag = await res.json()
		expect(body.key).toEqual(hahtagKey)
	})
	it('/hashtags/all endpoint localize hashtag', async () => {
		const hahtagKey = 'art'
		let res = await fetch(`${API_URL}/hashtags/${hahtagKey}`, setLangHeader('en'))
		let body: Hashtag = await res.json()
		expect(body.name).toEqual(hahtagKey)
		res = await fetch(`${API_URL}/hashtags/${hahtagKey}`, setLangHeader('es'))
		body = await res.json()
		expect(body.name).toEqual('arte')
	})
})
