export const langs: { [key in LocaleKey]: { key: LocaleKey; name: LocaleName } } = {
	ar: {
		key: 'ar',
		name: 'arabic',
	},
	az: {
		key: 'az',
		name: 'azerbaijan',
	},
	bs: {
		key: 'bs',
		name: 'bosnian',
	},
	bg: {
		key: 'bg',
		name: 'bulgarian',
	},
	ca: {
		key: 'ca',
		name: 'catalan',
	},
	et: {
		key: 'et',
		name: 'estonian',
	},
	eu: {
		key: 'eu',
		name: 'basque',
	},
	zh: {
		key: 'zh',
		name: 'chinese',
	},
	hr: {
		key: 'hr',
		name: 'croatian',
	},
	cs: {
		key: 'cs',
		name: 'czech',
	},
	sk: {
		key: 'sk',
		name: 'slovak',
	},
	da: {
		key: 'da',
		name: 'danish',
	},
	nl: {
		key: 'nl',
		name: 'dutch',
	},
	en: {
		key: 'en',
		name: 'english',
	},
	es: {
		key: 'es',
		name: 'spanish',
	},
	eo: {
		key: 'eo',
		name: 'esperanto',
	},
	fi: {
		key: 'fi',
		name: 'finnish',
	},
	fr: {
		key: 'fr',
		name: 'french',
	},
	gl: {
		key: 'gl',
		name: 'galician',
	},
	hi: {
		key: 'hi',
		name: 'hindi',
	},
	hu: {
		key: 'hu',
		name: 'hungarian',
	},
	ka: {
		key: 'ka',
		name: 'georgian',
	},
	ko: {
		key: 'ko',
		name: 'korean',
	},
	ku: {
		key: 'ku',
		name: 'kurdish',
	},
	lv: {
		key: 'lv',
		name: 'latvian',
	},
	de: {
		key: 'de',
		name: 'german',
	},
	el: {
		key: 'el',
		name: 'greek',
	},
	he: {
		key: 'he',
		name: 'hebrew',
	},
	id: {
		key: 'id',
		name: 'indonesian',
	},
	it: {
		key: 'it',
		name: 'italian',
	},
	ja: {
		key: 'ja',
		name: 'japanese',
	},
	no: {
		key: 'no',
		name: 'norwegian',
	},
	pt: {
		key: 'pt',
		name: 'portuguese',
	},
	pl: {
		key: 'pl',
		name: 'polish',
	},
	ru: {
		key: 'ru',
		name: 'russian',
	},
	ro: {
		key: 'ro',
		name: 'romanian',
	},
	sv: {
		key: 'sv',
		name: 'swedish',
	},
	tr: {
		key: 'tr',
		name: 'turkish',
	},
	uk: {
		key: 'uk',
		name: 'ukrainian',
	},
}
