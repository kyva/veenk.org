import { debugError } from '#shared/utils/debug'
import alternativesService from '~/server/services/alternatives.service'

export default defineEventHandler(async () => {
	try {
		const items = await alternativesService.getAll()
		return items
	} catch (err) {
		debugError(err)
		throw createError({
			statusCode: 400,
			message: 'Could not fetch all Alternatives!'
		})
	}
})
