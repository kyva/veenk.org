import { availableLangs, localizedProps } from '~~/constants/localization'

// function getTranslatableProperties(data: any): string[] {
// 	const translatableProperties = new Set<string>()

// 	for (const [key, value] of Object.entries(data)) {
// 		const typedValue = value as { en: string }
// 		if (typeof value === 'object' && typedValue.en) {
// 			translatableProperties.add(key)
// 		}
// 	}
// 	return Array.from(translatableProperties)
// }

// biome-ignore lint/suspicious/noExplicitAny: <explanation>
export const formatLocalizedTexts = <T>(obj: any, table: LocalizedTables, lang: LocaleKey): T => {
	const result = { ...obj }
	for (const prop of localizedProps[table]) {
		if (!prop.includes('.')) {
			if (!result[prop]) continue
			result[prop] = result[prop][lang]
		} else if (!prop.includes('[]')) {
			const [parent, child] = prop.split('.')
			result[parent][child] = result[parent][child][lang]
		} else {
			const [parent, child] = prop.split('[].')
			if (!result[parent]) continue
			for (const item of result[parent]) {
				if (!item[child]) continue
				item[child] = item[child][lang]
			}
		}
	}
	return result
}

export const getLangFromHeaders = (headers: Headers) => {
	const userLanguage = headers.get('accept-language') as LocaleKey
	return availableLangs.includes(userLanguage) ? userLanguage : 'en'
}
