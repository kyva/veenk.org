import { describe, it, expect } from 'vitest'
import { API_URL } from './setup'

describe('Finances endpoint', async () => {
	it('/finances/internal-list endpoint status 200', async () => {
		const res = await fetch(`${API_URL}/finances/internal-list`)
		expect(res.status).toEqual(200)
	})
	it('/finances/stats endpoint status 200', async () => {
		const res = await fetch(`${API_URL}/finances/stats`)
		expect(res.status).toEqual(200)
	})
})
