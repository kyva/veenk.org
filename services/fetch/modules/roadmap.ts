const RoadmapModule = () => {
	const { getReq } = useCustomFetch()
	const MODULE_URL = '/roadmap'

	const getRoadmap = () => getReq<Roadmap[]>(`${MODULE_URL}/all`)

	return {
		getRoadmap
	}
}

export default RoadmapModule
