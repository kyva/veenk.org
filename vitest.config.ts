import { defineVitestConfig } from '@nuxt/test-utils/config'

export default defineVitestConfig({
	test: {
		name: 'api',
		root: './tests/api',
		environment: 'node'
	}
})
