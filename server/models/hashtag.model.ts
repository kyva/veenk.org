import mongoose from 'mongoose'
const Schema = mongoose.Schema

const schema = new Schema(
	{
		key: {
			type: String,
			required: true,
			unique: true
		},
		count: Number,
		related: [String],
		name: Schema.Types.Mixed,
		label: Schema.Types.Mixed
	},
	{
		id: false,
		toJSON: { virtuals: true }
	}
)

export const HashtagModel = mongoose.model('Hashtag', schema)
