import mongoose from 'mongoose'
const Schema = mongoose.Schema

const schema = new Schema<Roadmap>(
	{
		name: {
			type: Schema.Types.Mixed,
			required: true
		},
		date: String,
		dateDisplay: String,
		isActive: Boolean,
		comingSoon: Boolean,
		future: Boolean,
		description: Schema.Types.Mixed
	},
	{
		id: false,
		toJSON: { virtuals: true },
		collection: 'roadmap'
	}
)

export const RoadmapModel = mongoose.model('Roadmap', schema)
