import type { Component } from 'vue'
import Veenk from '../Veenk.vue'
import OpenCollective from './OpenCollective.vue'
import Liberapay from './Liberapay.vue'
import Patreon from './Patreon.vue'
import GithubSponsors from './GithubSponsors.vue'
import Custom from './Custom.vue'
import Buymeacoffee from './Buymeacoffee.vue'
import Paypal from './Paypal.vue'
import Kofi from './Kofi.vue'

const icons: { [key in DonationOptions]: Component } = {
	veenk: Veenk,
	opencollective: OpenCollective,
	liberapay: Liberapay,
	custom: Custom,
	patreon: Patreon,
	'github-sponsors': GithubSponsors,
	buymeacoffee: Buymeacoffee,
	paypal: Paypal,
	'ko-fi': Kofi,
}

export default icons
