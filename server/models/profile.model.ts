import mongoose from 'mongoose'
const Schema = mongoose.Schema

const schema = new Schema(
	{
		username: {
			type: String,
			required: true,
			unique: true
		},
		name: String,
		avatar: String,
		patronOf: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Projects'
			}
		],
		teamOf: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Projects'
			}
		]
	},
	{
		id: false,
		toJSON: { virtuals: true },
		timestamps: true
	}
)

export const ProfileModel = mongoose.model('Profile', schema)
