import hashtagService from '~/server/services/hashtags.service'

export default defineEventHandler(async (event) => {
	const key = event.context.params?.hashtagKey

	if (!key) {
		throw createError({
			statusCode: 400,
			message: 'Bad request, hashtag key is required!'
		})
	}

	const filters = {
		key
	}

	const lang = getLangFromHeaders(event.headers)

	try {
		const item = await hashtagService.getHashtag(filters, lang)
		if (!item) {
			throw createError({
				statusCode: 404,
				message: `Could not find any hashtag with ${key} key!`
			})
		}
		return item
	} catch (err) {
		console.error('Error: ', err)
		throw createError({
			statusCode: 500,
			message: `Server error while fetching ${key} hashtag!`
		})
	}
})
