import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useNewsStore = defineStore('news', () => {
	const news = ref<New[]>([])

	async function setNews(newsData: New[]) {
		news.value = newsData
	}

	return {
		news,
		setNews,
	}
})
