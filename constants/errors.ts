export const ERRORS = {
	PROJECT_NOT_FOUND: 'PROJECT_NOT_FOUND',
	PAGE_NOT_FOUND: 'PAGE_NOT_FOUND'
} as const
