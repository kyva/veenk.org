export const debounce = (method: () => void, delay: number) => {
	// @ts-expect-error
	clearTimeout(method._tId)
	// @ts-expect-error
	method._tId = setTimeout(() => {
		method()
	}, delay)
}
