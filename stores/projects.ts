import { defineStore } from 'pinia'
import { ref } from 'vue'
import { RECOMMENDED_ALTERNATIVES_QUANTITY, RECOMMENDED_HASHTAGS_QUANTITY } from '~/constants/filters'
import { debugError } from '~/shared/utils/debug'

export const useProjectsStore = defineStore('projects', () => {
	const { $api } = useNuxtApp()
	const router = useRouter()

	const params = reactive<ProjectParams>({
		page: 0,
		platforms: [],
		badges: []
	})

	const projectsList = ref<ProjectList>([])
	const allProjects = ref<ProjectMinimal[]>([])
	const selectedView = ref<Views>('user')
	const isFetching = ref<boolean>(false)
	const activePlatforms = ref<PlatformTypeRaw[]>([])
	// const activeBadges = ref<BadgeType[]>([])
	const alternatives = ref<Alternative[]>([])
	const activeAlternative = ref<Alternative>()
	const activeHashtag = ref<Hashtag>()
	const hashtags = ref<Hashtag[]>([])
	const badges = ref<BadgeType[]>([])
	const platforms = ref<PlatformAPI[]>([])
	const projectsDonationInfo = ref<ProjectDonationInfo[]>([])

	const setAllProjects = (val: ProjectMinimal[]) => {
		allProjects.value = val
	}

	const getProjectDonationInfo = (slug: string) => {
		return projectsDonationInfo.value.find((item) => item.slug === slug)
	}
	const addProjectToDonationInfo = (item: ProjectDonationInfo) => {
		projectsDonationInfo.value.push(item)
	}

	const applyFilters = (filters: ProjectFilters) => {
		if (filters.activePlatforms) {
			params.platforms = filters.activePlatforms
		}
		if (filters.activeBadges) {
			params.badges = filters.activeBadges
		}

		fetchProjectsList()
	}

	const recommendedHashtags = (searchTerm: string): Hashtag[] => {
		const search = clearSearchString(searchTerm)
		let results = []
		if (search.length > 2) {
			for (const hashtag of hashtags.value) {
				if (hashtag.name.includes(search)) {
					results.push(hashtag)
				}
			}
		} else {
			results = hashtags.value
		}
		return results.slice(0, RECOMMENDED_HASHTAGS_QUANTITY)
	}

	const recommendedAlternatives = () => {
		const records = activeAlternative.value
			? alternatives.value.filter((item) => item.slug !== activeAlternative.value?.slug)
			: alternatives.value
		return records.slice(0, RECOMMENDED_ALTERNATIVES_QUANTITY)
	}

	const fetchNextPage = async () => {
		params.page = params.page + 1
		isFetching.value = true
		try {
			const newProjects = await $api.projects.getProjectsList(params)
			const result = [...projectsList.value, ...newProjects]
			projectsList.value = result
		} catch (err) {
			console.log('Error: ', err)
		}
		isFetching.value = false
	}

	const togglePlatformActive = (type: PlatformTypeRaw) => {
		if (activePlatforms.value.includes(type)) {
			const without = activePlatforms.value.filter((val) => val !== type)
			activePlatforms.value = without
		} else {
			activePlatforms.value = [...activePlatforms.value, type]
		}
		fetchProjectsList()
	}

	function setActiveAlternative(val: Alternative | undefined) {
		activeAlternative.value = val
		params.alternative = val?.slug
		fetchProjectsList()
	}

	const setActiveHashtag = async (hashtagSlug: string | undefined) => {
		params.hashtag = hashtagSlug
		fetchProjectsList()
		if (hashtagSlug) {
			const hashtag = await $api.projects.getHashtagBySlug(hashtagSlug)
			activeHashtag.value = hashtag
		} else {
			activeHashtag.value = undefined
			// TODO: remove and optimize
			router.push('/projects')
		}
	}

	const fetchTags = async () => {
		try {
			badges.value = await $api.projects.getBadgesList()
		} catch (err) {
			debugError(err)
		}
		try {
			alternatives.value = await $api.projects.getAlternatives()
		} catch (err) {
			debugError(err)
		}
		try {
			platforms.value = await $api.projects.getPlataforms()
		} catch (err) {
			debugError(err)
		}
		try {
			const result = await $api.projects.getHashtags()
			hashtags.value = result
			return result
		} catch (err) {
			debugError(err)
		}

		return false
	}

	const fetchProjectsList = async () => {
		resetParams()
		isFetching.value = true
		const newProjects = await $api.projects.getProjectsList(params)
		projectsList.value = newProjects
		isFetching.value = false
		return newProjects
	}

	const resetParams = () => {
		params.page = 0
	}

	// const activePlatformsSaved = localStorage.getItem('activePlatforms')

	// if (activePlatformsSaved) {
	// 	activePlatforms.value = JSON.parse(activePlatformsSaved)
	// }

	// watch(
	// 	activePlatforms,
	// 	(val) => {
	// 		localStorage.setItem('activePlatforms', JSON.stringify(val))
	// 	},
	// 	{
	// 		deep: true,
	// 	},
	// )

	const getProjectBySlug = (slug: string) => allProjects.value.find((project) => project.slug === slug)

	const fetchProjectProfile = async (slug: string): Promise<ProjectProfile> => {
		const result = await $api.projects.getProject(slug)
		return result
	}

	return {
		params,
		projectsList,
		allProjects,
		setAllProjects,
		fetchProjectsList,
		selectedView,
		applyFilters,
		setActiveHashtag,
		isFetching,
		getProjectDonationInfo,
		addProjectToDonationInfo,
		alternatives,
		activeAlternative,
		activeHashtag,
		setActiveAlternative,
		fetchNextPage,
		fetchTags,
		togglePlatformActive,
		getProjectBySlug,
		fetchProjectProfile,
		badges,
		platforms,
		hashtags,
		recommendedHashtags,
		recommendedAlternatives
	}
})
