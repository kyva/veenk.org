import { AlternativeModel } from '../models/alternative.model'

async function getAll() {
	const alternatives = await AlternativeModel.find(
		{},
		{
			slug: 1,
			name: 1,
			img: 1,
			count: { $size: '$libreProjectsIds' }
		}
	)
	return alternatives
}

const addFromObj = (data: unknown) => AlternativeModel.insertMany(data)

export default {
	getAll,
	addFromObj
}
