const instances: Instance[] = [
	{
		type: 'meetiko',
		software: 'Sharkey',
		softwareUrl: 'https://activitypub.software/TransFem-org/Sharkey',
		url: 'meetiko.org',
	},
	{
		type: 'luzeed',
		software: 'Pixelfed',
		softwareUrl: 'https://pixelfed.org/',
		url: 'luzeed.org',
	},
	{
		type: 'veedeo',
		software: 'Peertube',
		softwareUrl: 'https://joinpeertube.org/',
		url: 'veedeo.org',
	},
	{
		type: 'peeko',
		software: 'XMPP',
		softwareUrl: 'https://xmpp.org/',
		url: 'peeko.chat',
	},
]

export default instances
