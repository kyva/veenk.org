import { RoadmapModel } from '../models/roadmap.model'

interface roadmapFilters {
	isActive?: boolean
}

interface getAllProps {
	filters: roadmapFilters
	lang: LocaleKey
}

async function getAll({ filters = {}, lang }: getAllProps) {
	const documents = await RoadmapModel.find(filters)
	return documents.map((doc) => formatLocalizedTexts(doc.toObject(), 'roadmap', lang))
}

const addFromObj = (data: unknown) => RoadmapModel.insertMany(data)

export default {
	getAll,
	addFromObj
}
