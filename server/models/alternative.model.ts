import mongoose from 'mongoose'
const Schema = mongoose.Schema

const schema = new Schema(
	{
		slug: {
			type: String,
			required: true,
			unique: true
		},
		name: {
			type: String,
			required: true
		},
		img: {
			type: String,
			required: true
		},
		libreProjectsIds: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'projects'
			}
		]
	},
	{
		id: false,
		toJSON: { virtuals: true },
		timestamps: true
	}
)

export const AlternativeModel = mongoose.model('Alternative', schema)
