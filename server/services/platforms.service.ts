import { PlatformModel } from '../models/platform.model'

async function getAll() {
	return await PlatformModel.find({})
}

export default {
	getAll
}
