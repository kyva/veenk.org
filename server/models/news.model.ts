import mongoose from 'mongoose'
const Schema = mongoose.Schema

const schema = new Schema(
	{
		text: String,
		type: String,
		startTime: Date,
		endTime: Date
	},
	{
		id: false,
		toJSON: { virtuals: true }
	}
)

export const NewsModel = mongoose.model('News', schema)
