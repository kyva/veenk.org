export default [
	{
		code: 'en',
		name: 'English'
	},
	{
		code: 'es',
		name: 'Spanish'
	},
	{
		code: 'nl',
		name: 'Dutch'
	}
]
