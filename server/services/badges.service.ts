import { BadgeModel } from '../models/badge.model'

async function getAll() {
	return await BadgeModel.find({})
}

export default {
	getAll
}
