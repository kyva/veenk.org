<div align="center" >
<p>  
</p>
<img src="./public/veenk-full-logo.svg" alt="Meetiko logo" width="340" />
</div>
<p></p>

[![LiberaPay](https://img.shields.io/liberapay/receives/kyva.svg?logo=liberapay)](https://liberapay.com/kyva/donate)
[![Woodpecker](https://ci.veenk.org/api/badges/1/status.svg)](https://ci.veenk.org/api/badges/1/status.svg)

# Veenk Official Website

This is the official website of the [Veenk](https://veenk.org) project that aims to build and boost free software projects.

You will find a list of libre software projects to help people find the right alternative.

## Technologies used

- Nuxt
- Vue
- Pinia
- Sass
- i18next
- Pnpm
- typescript
- mongoose

## Setup

### Dependencies

Install [pnpm](https://pnpm.io/installation) and then run `pnpm install`

### Database

You need a running MongoDB, for which we recommend using [Podman](https://podman.io/docs/installation) and [Podman Desktop](https://podman-desktop.io/docs/installation), and optionally [MongoDB Compass](https://www.mongodb.com/try/download/compass) to explore and manipulate the data.

```
podman pull docker.io/mongo:latest
podman run --name mongodb -p 27017:27017 docker.io/library/mongo:latest
```

Copy the `.env.example` file to `.env` and change the `MONGODB_URI` value inside it according to your needs (by default it is `mongodb://localhost:27017/veenk_dev`).

Finally run `pnpm seed:simple` to populate the database.

### Start Veenk

Run `pnpm dev` to start Veenk. The website will be available at http://localhost:3000 and you can access the api through http://localhost:3000/api/

To build Veenk for production run `pnpm build`, then start it with `node .output/server/index.mjs`.

---

managers folder aut-import all components that are a manager (\*Manager.vue)

### Contribute

Please, before making a PR read the `CONTRIBUTING.md` file and follow the indications.
