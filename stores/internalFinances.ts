import { defineStore } from 'pinia'
import { reactive, ref } from 'vue'

export const useInternalFinancesStore = defineStore('internalFinances', () => {
	const { $api } = useNuxtApp()
	const { t } = useI18n()

	const stats = reactive<VeenkStats>({
		actualCollected: null,
		actualGoal: null,
		totalCollected: 0,
		totalGoal: 0
	})
	const label = ref(' ')

	const internalFinancesList = ref<InternalFinances[]>([])

	// const doublePlusOne = computed<number>(() => double.value + 1)

	async function setStats(financesStats: FinancesStats) {
		stats.actualCollected = financesStats.actualCollected
		stats.actualGoal = financesStats.actualGoal
		stats.totalCollected = financesStats.totalCollected
		stats.totalGoal = financesStats.totalGoal
		label.value = `${t(`months.${financesStats.month - 1}`)} ${financesStats.year}`
		// label.value = `${$t('months.' + (veenkStats.month - 1))} of ${veenkStats.year}`
	}

	async function setList(data: InternalFinances[]) {
		internalFinancesList.value = data
	}

	const fetchStats = async () => {
		const financesStats = await $api.internalFinances.getStats()
		setStats(financesStats)
		return financesStats
	}

	const fetchList = async () => {
		const data = await $api.internalFinances.getList()
		setList(data)
		return data
	}

	return {
		label,
		stats,
		internalFinancesList,
		fetchStats,
		fetchList,
		setStats
	}
})
