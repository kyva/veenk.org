import projectService from '~/server/services/project.service'
import { debugError } from '#shared/utils/debug'

export default defineEventHandler(async (event) => {
	const slug = event.context.params?.projectSlug

	if (!slug) {
		throw createError({
			statusCode: 400,
			message: 'Bad request, slug is required to fetch project donation info!'
		})
	}

	const filters = {
		slug,
		isActive: true,
		isReady: true
	}

	let project: ProjectDonationInfo | null
	try {
		project = await projectService.getProjectDonationInfo({ filters })
		if (project === null) {
			throw createError({
				statusCode: 404,
				message: 'Project donation info not found!'
			})
		}
	} catch (err) {
		debugError(err)
		throw createError({
			statusCode: 500,
			message: 'Server error when fetching Project donation info!'
		})
	}
	return project
})
