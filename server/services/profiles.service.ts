import { ProfileModel } from '../models/profile.model'

async function getAll() {
	return await ProfileModel.find({})
}

export default {
	getAll
}
