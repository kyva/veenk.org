import { HashtagModel } from '../models/hashtag.model'

interface getAllProps {
	lang: LocaleKey
}

async function getAll({ lang }: getAllProps) {
	const documents = await HashtagModel.find()
	return documents.map((doc) => formatLocalizedTexts(doc.toObject(), 'hashtags', lang))
}

async function getHashtag(filters: { key: string }, lang: LocaleKey) {
	const doc = await HashtagModel.findOne(filters)
	if (!doc) return
	return formatLocalizedTexts<Hashtag>(doc.toObject(), 'hashtags', lang)
}

const addFromObj = (data: unknown) => HashtagModel.insertMany(data)

export default {
	getAll,
	getHashtag,
	addFromObj
}
