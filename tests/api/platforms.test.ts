import { describe, it, expect } from 'vitest'
import { API_URL } from './setup'

describe('Platforms endpoint', async () => {
	it('/platforms/all endpoint status 200', async () => {
		const res = await fetch(`${API_URL}/platforms/all`)
		expect(res.status).toEqual(200)
	})
})
