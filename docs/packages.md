# Libraries & Helpers (DevExp)

- @vueuse: general composables for vue

# Backend

# Testing

- bson: to generate valid mongodb ObjectId for testing

# Other

- [branch: "add-prisma"] Prisma: as a ORM, help you create models for the database.
