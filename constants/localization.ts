export const availableLangs: LocaleKey[] = ['en', 'es']

export const localizedProps = {
	projects: ['description', 'screenshots[].alt', 'tags[].name'],
	roadmap: ['name', 'description'],
	internalFinances: ['expenses[].name', 'takings[].name'],
	hashtags: ['name', 'label']
}

declare global {
	type LocalizedTables = keyof typeof localizedProps
}
