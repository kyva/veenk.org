type Either<E, A> = Left<E> | Right<A>

interface Left<E> {
	_tag: 'Left'
	left: E
}

interface Right<A> {
	_tag: 'Right'
	right: A
}

const left = <E, A = never>(e: E): Either<E, A> => ({
	_tag: 'Left',
	left: e,
})

const right = <A, E = never>(a: A): Either<E, A> => ({
	_tag: 'Right',
	right: a,
})

function divideTwoIfEven(num: number): Either<string, number> {
	if (num === 0) {
		return left('cannot divide by zero')
	}
	if (num % 2 !== 0) {
		return left('num is not even')
	}
	return right(2 / num)
}

const isLeft = <E, A>(x: Either<E, A>): x is Left<E> => x._tag === 'Left'

type Increment = (x: number) => number
const increment: Increment = (x) => x + 1

type Compose = <A, B, C>(f: (x: B) => C, g: (x: A) => B) => (x: A) => C
const compose: Compose = (f, g) => (x) => f(g(x))

const composed = compose((x) => (isLeft(x) ? x : right(increment(x.right))), divideTwoIfEven)
