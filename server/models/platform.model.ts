import mongoose from 'mongoose'
const Schema = mongoose.Schema

const schema = new Schema(
	{
		type: String,
		name: String
	},
	{
		id: false,
		toJSON: { virtuals: true }
	}
)

export const PlatformModel = mongoose.model('Platform', schema)
