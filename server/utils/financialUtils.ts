import moment from 'moment'

interface ObjectWithNumbs {
	[key: string]: number | string
}

const calculateTotal = (list: ObjectWithNumbs[], key = 'amount') =>
	list.reduce((acc, currentVal) => acc + (typeof currentVal[key] === 'number' ? (currentVal[key] as number) : 0), 0)

const calculateAllTime = (finances: InternalFinances[]) => {
	return finances.reduce(
		(acc, currentVal) => ({
			expenses: acc.expenses + calculateTotal(currentVal.expenses.map((val) => ({ amount: val.amount }))),
			takings: acc.takings + calculateTotal(currentVal.takings)
		}),
		{ expenses: 0, takings: 0 }
	)
}

const formatInvoices = (list: FinanceRecord[]) => {
	return list.map((item) => ({
		name: item.name,
		source: item.source,
		sourceName: item.sourceName,
		amount: item.amount,
		date: moment(item.date).format('D.M.YYYY')
	}))
}

export const financialUtils = {
	formatInvoices,
	calculateAllTime,
	calculateTotal
}
