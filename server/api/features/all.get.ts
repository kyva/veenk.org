import platformsService from '~/server/services/platforms.service'
import { debugError } from '#shared/utils/debug'

export default defineEventHandler(async () => {
	try {
		const items = await platformsService.getAll()
		return items
	} catch (err) {
		debugError(err)
		throw createError({
			statusCode: 400,
			message: 'Could not fetch all Platforms!'
		})
	}
})
