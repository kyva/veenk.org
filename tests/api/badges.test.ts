import { describe, it, expect } from 'vitest'
import { API_URL } from './setup'

describe('Badges endpoint', async () => {
	it('/badges/all endpoint status 200', async () => {
		const res = await fetch(`${API_URL}/badges/all`)
		expect(res.status).toEqual(200)
	})
})
