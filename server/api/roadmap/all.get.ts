import roadmapService from '~/server/services/roadmap.service'
import { debugError } from '~/shared/utils/debug'

export default defineEventHandler(async (event) => {
	const filters = {
		isActive: true
	}
	const lang = getLangFromHeaders(event.headers)

	try {
		const items = await roadmapService.getAll({ filters, lang })
		return items
	} catch (err) {
		debugError(err)
		throw createError({
			statusCode: 500,
			statusMessage: 'Server error when fetching roadmap items!'
		})
	}
})
