import { debugError } from '~/shared/utils/debug'

const ProjectsModule = () => {
	const { getReq, fetch } = useCustomFetch()
	const MODULE_URL = '/projects'

	const getProjectsList = async (params: ProjectParams) => {
		let paramsUrl = ''
		paramsUrl = params.page ? `${paramsUrl}&page=${params.page}` : paramsUrl
		paramsUrl = params.hashtag ? `${paramsUrl}&hashtag=${params.hashtag}` : paramsUrl
		paramsUrl = params.alternative ? `${paramsUrl}&alternative=${params.alternative}` : paramsUrl
		paramsUrl =
			params.badges && params.badges.length > 0
				? `${paramsUrl}&hasBadge=${encodeURIComponent(params.badges.join(' '))}`
				: paramsUrl
		paramsUrl =
			params.platforms && params.platforms.length > 0
				? `${paramsUrl}&hasPlatform=${encodeURIComponent(params.platforms.join(' '))}`
				: paramsUrl

		paramsUrl = paramsUrl.replace(/^&/, '')

		// if (params.alternative) {
		// 	getReq<Project[]>(`/alternatives/${params.alternative}`).then((projects) => {
		// 		resolve(projects)
		// 	})
		// }

		let projectList: ProjectList = []
		try {
			projectList = await fetch<ProjectList>(`${MODULE_URL}/list?${paramsUrl}`, { method: 'GET' })
		} catch (err) {
			debugError(err)
		}
		return projectList
	}

	const getProjectDonationInfoBySlug = async (slug: string) => {
		try {
			const data = await getReq<ProjectDonationInfo>(`${MODULE_URL}/${slug}/donation-info`)
			return data
		} catch (error) {
			console.error('Error: ', error)
			throw createError({
				message: 'Error fetching project donation info. (6760c6a9-sbb8)'
			})
		}
	}

	const getAllProjects = async () => {
		try {
			const data = await getReq<ProjectMinimal[]>(`${MODULE_URL}/all`)
			return data
		} catch {
			throw createError({
				message: 'fetching all projects. (5750c6a9-fba8)'
			})
		}
	}

	const getProject = (slug: string) => getReq<ProjectProfile>(`/projects/${slug}/profile`)

	// GOOD ONE
	// const getAllProjects = () => customFetch(PROJECTS_URL, { method: 'GET' })

	const getAlternatives = () => getReq<Alternative[]>('/alternatives/all')
	const getPlataforms = () => getReq<PlatformAPI[]>('/platforms/all')
	const getBadgesList = () =>
		getReq<{ key: string }[]>('/badges/all').then((badges) => badges.map((item) => item.key as BadgeType))

	// export const getHashtag: (key: string) => Promise<Hashtag> = (key) =>
	// 	new Promise((resolve) => {
	// 		getReq<Hashtag>(`/hashtags/${key}`).then((hashtag) => {
	// 			resolve(hashtag)
	// 		})
	// 	})

	const getHashtags = () => getReq<Hashtag[]>('/hashtags/all')
	const getHashtagBySlug = (slug: string) => getReq<Hashtag>(`/hashtags/${slug}`)

	return {
		getProjectsList,
		getAllProjects,
		getAlternatives,
		getPlataforms,
		getBadgesList,
		getProject,
		getHashtags,
		getHashtagBySlug,
		getProjectDonationInfoBySlug
	}
}

export default ProjectsModule
