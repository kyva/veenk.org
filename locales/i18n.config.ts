import en from './en.json'
import es from './es.json'
import nl from './nl.json'

export default defineI18nConfig(() => ({
	legacy: false,
	locale: 'en',
	supportedLngs: ['es', 'en'],
	fallbackLocale: 'en',
	messages: {
		en,
		es,
		nl
	}
}))
