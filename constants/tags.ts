declare global {
	type BadgeType = (typeof BADGE_TYPE)[number]

	type Badges = {
		[key in BadgeType]: {
			icon: string
		}
	}

	type PlatformTypeRaw = (typeof PLATFORM_TYPES)[number]

	type Platforms = {
		[key in PlatformTypeRaw]: {
			name: string
			icon: string
		}
	}

	type PlatformType = PlatformTypeRaw | 'multiple'
}

const BADGE_TYPE = [
	'unique',
	'transparency',
	'savesThePlanet',
	'socialJustice',
	'accessible',
	'copyleft',
	'forum',
	'localization',
] as const

export const badges: Badges = {
	unique: {
		icon: 'ph:heartbeat',
	},
	transparency: {
		icon: 'ph:chart-pie-slice',
	},
	savesThePlanet: {
		icon: 'ph:leaf',
	},
	socialJustice: {
		icon: 'ph:hand-fist',
	},
	accessible: {
		icon: 'ph:person',
	},
	copyleft: {
		icon: 'ph:copyleft',
	},
	forum: {
		icon: 'ph:lifebuoy',
	},
	localization: {
		icon: 'ph:translate',
	},
}

const PLATFORM_TYPES = [
	'linux',
	'mac',
	'windows',
	'android',
	'ios',
	'web',
	'self-host',
	'dev',
	'tvos',
	'bios',
	'x86-64',
	'arm64',
	'pwa',
] as const

export const platforms: Platforms = {
	linux: {
		name: 'GNU/Linux',
		icon: 'ph:linux-logo-fill',
	},
	android: {
		name: 'Android',
		icon: 'ph:android-logo-fill',
	},
	mac: {
		name: 'Mac',
		icon: 'ph:apple-logo-fill',
	},
	windows: {
		name: 'Windows',
		icon: 'ph:windows-logo-fill',
	},
	web: {
		name: 'Web',
		icon: 'ph:browser-fill',
	},
	ios: {
		name: 'iOS',
		icon: 'ph:app-store-logo-fill',
	},
	'self-host': {
		name: 'Self-host',
		icon: 'ph:hard-drives-fill',
	},
	dev: {
		name: 'Dev',
		icon: 'ph:code-fill',
	},
	tvos: {
		name: 'tvOS',
		icon: 'ph:television-simple-fill',
	},
	bios: {
		name: 'Bios',
		icon: 'ph:cpu-fill',
	},
	'x86-64': {
		name: 'x86-64',
		icon: 'ph:computer-tower-fill',
	},
	arm64: {
		name: 'arm64',
		icon: 'ph:hard-drive-fill',
	},
	pwa: {
		name: 'pwa',
		icon: 'ph:device-mobile-fill',
	},
	// multiple: {
	// 	name: 'multiple',
	// 	icon: 'ph:circles-three-fill',
	// },
}
