import type { Component } from 'vue'
import Codeberg from './Codeberg.vue'
import Github from './Github.vue'
import Gitlab from './Gitlab.vue'
import GitlabSelfHost from './GitlabSelfHost.vue'
import Forgejo from './Forgejo.vue'
import Custom from './Custom.vue'

const icons: { [key in RepositoryType]: Component } = {
	github: Github,
	gitlab: Gitlab,
	'gitlab-selfhost': GitlabSelfHost,
	gitea: Custom,
	codeberg: Codeberg,
	forgejo: Forgejo,
	sourceforge: Custom,
	gerrit: Custom,
	custom: Custom,
}

export default icons
