import type { UpdateOneModel } from 'mongoose'
import { AlternativeModel } from '../models/alternative.model'
import { HashtagModel } from '../models/hashtag.model'
import { ExternalProjectDataModel, ProjectModel } from '../models/project.model'

const publicFields = {
	importHelper: 0
}

type GetAll = (props: { filters: ProjectFiltersAPI }) => Promise<ProjectMinimal[]>

const getAll: GetAll = async ({ filters }) => {
	const select: MongoSelect<ProjectMinimal> = {
		_id: 0,
		slug: 1,
		name: 1,
		logoUrl: 1
	}
	const documents = await ProjectModel.find(filters).select(select)
	return documents.map((doc) => doc.toObject())
}

type GetProjectDonationInfo = (props: { filters: ProjectFiltersBySlug }) => Promise<ProjectDonationInfo | null>

const getProjectDonationInfo: GetProjectDonationInfo = async ({ filters }) => {
	const select: MongoSelect<ProjectDonationInfo> = {
		_id: 0,
		slug: 1,
		name: 1,
		logoUrl: 1,
		fromOrganization: 1,
		members: 1,
		finances: 1,
		webUrl: 1,
		team: 1,
		created: 1
	}
	const project = await ProjectModel.findOne(filters).select(select)
	return project
}

type GetProjectListed = (props: {
	filters: ProjectsListFilters
	options: ProjectListOptions
	pageFilter: ProjectPageFilter
	lang: LocaleKey
}) => Promise<ProjectList[]>

const getListed: GetProjectListed = async ({ filters, options, pageFilter, lang }) => {
	if (pageFilter.alternativeTo !== null) {
		const alternative = await AlternativeModel.findOne({ slug: pageFilter.alternativeTo })
		filters.alternativeTo = alternative ? alternative._id : undefined
	}
	if (pageFilter.hashtag !== null) {
		const hashtag = await HashtagModel.findOne({ key: pageFilter.hashtag })
		filters.tags = hashtag ? { $in: [hashtag._id] } : undefined
	}

	const documents = await ProjectModel.aggregate()
		.match(filters)
		.sort({ karma: 'desc' })
		.lookup({
			from: 'hashtags',
			localField: 'tags',
			foreignField: '_id',
			as: 'tags'
		})
		.lookup({
			from: 'external_projects_data',
			localField: 'externalData',
			foreignField: '_id',
			as: 'externalData'
		})
		.lookup({
			from: 'alternatives',
			localField: 'alternativeTo',
			foreignField: '_id',
			as: 'alternativeTo'
		})
		.project(publicFields)
		.facet({ data: [{ $skip: options.skip }, { $limit: options.limit }] })

	return documents[0].data.map((doc: ProjectCard | ProjectMinimalWithReady) =>
		formatLocalizedTexts<Array<ProjectCard | ProjectMinimalWithReady>>(doc, 'projects', lang)
	)
}

type GetProjectProfile = (props: {
	filters: ProjectFiltersBySlug
	lang: LocaleKey
}) => Promise<ProjectProfile | undefined>

const getProjectProfile: GetProjectProfile = async ({ filters, lang }) => {
	const doc = await ProjectModel.findOne(filters)
		.select(publicFields)
		.populate({
			path: 'alternativeTo',
			select: '-libreProjectsIds'
		})
		.populate({
			path: 'tags',
			select: 'key'
		})
		.populate({
			path: 'externalData',
			select: 'lastRepoImport'
		})
	if (!doc) return
	return formatLocalizedTexts(doc.toObject(), 'projects', lang)
}

async function updateExternalRepoData({
	projectId,
	externalData
}: {
	projectId: string
	externalData: { repos: RepoData }
}): Promise<UpdateOneModel | null> {
	const data = {
		date: new Date(),
		...externalData
	}
	return await ExternalProjectDataModel.findOneAndUpdate(
		{ projectId },
		{
			lastRepoImport: data,
			$push: { history: data }
		}
	)
}

interface AddFromObjProps {
	data: unknown
	external?: boolean
}

const addFromObj = ({ data, external = false }: AddFromObjProps) => {
	if (external === true) return ExternalProjectDataModel.insertMany(data)
	return ProjectModel.insertMany(data)
}

export default {
	getAll,
	getProjectDonationInfo,
	getListed,
	getProjectProfile,
	updateExternalRepoData,
	addFromObj
}
