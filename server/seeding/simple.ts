import fs from 'fs';
import mongoose from 'mongoose';
import { resolve } from 'path';
import alternativesService from '../services/alternatives.service';
import featuresService from '../services/features.service';
import financesService from '../services/finances.service';
import hashtagsService from '../services/hashtags.service';
import projectService from '../services/project.service';
import roadmapService from '../services/roadmap.service';

const mongourl = process.env.MONGODB_URI

const connect = () => mongourl ? mongoose.connect(mongourl) : Promise.reject("ERROR: process.env.MONGODB_URI was not set")

const disconnect = () => {
	mongoose.disconnect()
}

const db = mongoose.connection
db.once('open', () => console.log('Connected to MongoDB'))

mongoose.Promise = global.Promise

connect().then(() => {
	seedDatabase()
		.then(() => {
			console.log(`Data correctly added to -> ${mongourl}`)
		})
		.catch((e: Error) => {
			console.error(e)
		})
		.finally(() => {
			disconnect()
		})
})
.catch((e: Error) => {
	console.error(e)
})

async function seedDatabase() {
	try {
		const alternatives = await readJSONFile('alternatives.json')
		await alternativesService.addFromObj(parseMongoIds(alternatives, 'alternatives'))
		console.log('alternatives: DONE')
		const external_projects_data = await readJSONFile('external_projects_data.json')
		await projectService.addFromObj({
			data: parseMongoIds(external_projects_data, 'external_projects_data'),
			external: true
		})
		console.log('external_projects_data: DONE')
		const features = await readJSONFile('features.json')
		await featuresService.addFromObj(parseMongoIds(features))
		console.log('features: DONE')
		const hashtags = await readJSONFile('hashtags.json')
		await hashtagsService.addFromObj(parseMongoIds(hashtags))
		console.log('hashtags: DONE')
		const internal_finances = await readJSONFile('internal_finances.json')
		await financesService.addFromObj(parseMongoIds(internal_finances))
		console.log('internal_finances: DONE')
		const projects = await readJSONFile('projects.json')
		await projectService.addFromObj({ data: parseMongoIds(projects, 'projects'), external: false })
		console.log('projects: DONE')
		const roadmap = await readJSONFile('roadmap.json')
		await roadmapService.addFromObj(parseMongoIds(roadmap))
		console.log('roadmap: DONE')
	} catch (error) {
		throw error as Error
	}
}

type ModelType = 'alternatives' | 'external_projects_data' | 'projects'

const parseMongoIds = (docs: any[], type?: ModelType): { _id: string }[] => {
	return docs.map((doc) => {
		if (type === 'alternatives') {
			doc.libreProjectsIds = parseArrayIds(doc.libreProjectsIds)
		}
		if (type === 'external_projects_data') {
			doc.projectId = parseId(doc.projectId)
		}
		if (type === 'projects') {
			if (doc.alternativeTo) doc.alternativeTo = parseId(doc.alternativeTo)
			if (doc.externalData) doc.externalData = parseId(doc.externalData)
			if (doc.features) doc.features = parseArrayIds(doc.features)
			if (doc.tags) doc.tags = parseArrayIds(doc.tags)
		}
		return parseMongoId(doc)
	})
}

const parseMongoId = (doc: { _id: { $oid: string } }): { _id: string } => ({ ...doc, _id: doc._id.$oid })
const parseArrayIds = (arr: { $oid: string }[]): string[] => arr.map(parseId)
const parseId = (item: { $oid: string }): string => item.$oid

async function readJSONFile(filename: string) {
	try {
		const filePath = resolve(`./server/seeding/mockData/${filename}`)
		const data = await fs.promises.readFile(filePath, 'utf8')
		return JSON.parse(data)
	} catch (error) {
		throw error as Error
	}
}
