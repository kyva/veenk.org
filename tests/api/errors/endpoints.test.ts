import { describe, it, expect } from 'vitest'
import { API_URL } from '../setup'

describe('Endpoint should return error', async () => {
	// Hashtags db should be empty
	it.skip('/hashtags/all endpoint status 500', async () => {
		const hahtagKey = 'fediverse'
		const res = await fetch(`${API_URL}/hashtags/${hahtagKey}`)
		const body = await res.json()
		expect(body.message).toEqual(`Server error while fetching ${hahtagKey} hashtag!`)
	})
})
