export const hasFee: { [key: string]: { percentage: string; fixed?: string; forOrg?: string } } = {
	paypal: {
		percentage: '2,9',
		forOrg: '2,9',
		fixed: '0,35',
	},
	patreon: {
		percentage: '5',
		fixed: '0,15',
	},
	buymeacoffee: {
		percentage: '5',
	},
	mollie: {
		percentage: '1,80',
		fixed: '0,25',
	},
	'github-sponsors': {
		percentage: '0',
		forOrg: '6',
	},
	stripe: {
		percentage: '1,5',
		forOrg: '1,5',
		fixed: '0,25',
	},
	donorbox: {
		percentage: '1.75',
	},
	hubspot: {
		percentage: '2.9',
	},
	clickandpledge: {
		percentage: '2,75',
		fixed: '0,20',
	},
}
