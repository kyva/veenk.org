import { describe, it, expect } from 'vitest'

describe('General test', async () => {
	it('test /hello endpoint', async () => {
		const res = await fetch('http://localhost:3000/api/hello')
		expect(res.status).toEqual(200)
	})
})
