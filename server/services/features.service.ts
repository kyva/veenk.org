import { FeatureModel } from '../models/feature.model'

const addFromObj = (data: unknown) => FeatureModel.insertMany(data)

export default {
	addFromObj
}
