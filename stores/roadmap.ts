import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useRoadmapStore = defineStore('roadmap', () => {
	const { $api } = useNuxtApp()
	const roadmap = ref<Roadmap[]>()

	async function fetchRoadmap() {
		const result = await $api.roadmap.getRoadmap()
		roadmap.value = result
		return result
	}

	const roadmapList = computed(() =>
		roadmap.value
			? roadmap.value.sort((a, b) => {
					const da = new Date(a.date).getTime()
					const db = new Date(b.date).getTime()

					return db - da
				})
			: []
	)

	return {
		roadmap,
		roadmapList,
		fetchRoadmap
	}
})
