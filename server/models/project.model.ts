import mongoose from 'mongoose'

const Schema = mongoose.Schema

// const LinkType = {
// 	type: String,
// 	url: String,
// }

const projectSchema = new Schema<ProjectDB>(
	{
		isActive: {
			type: Boolean,
			default: false
		},
		isReady: {
			type: Boolean,
			default: false
		},
		karma: Number,
		name: {
			type: String,
			required: true,
			unique: true
		},
		slug: {
			type: String,
			required: true,
			unique: true
		},
		logoUrl: {
			type: String,
			required: true
		},
		webUrl: {
			type: String,
			required: true
		},
		description: Schema.Types.Mixed,
		fullDescription: Schema.Types.Mixed,
		created: String,
		location: String,
		fromOrganization: {
			type: Boolean,
			default: false
		},
		organization: {
			name: String,
			url: String
		},
		tags: [
			{
				type: Schema.Types.ObjectId,
				ref: 'Hashtag'
			}
		],
		contactLinks: [Schema.Types.Mixed],
		socialLinks: [Schema.Types.Mixed],
		languages: [String],
		localizationSoftware: {
			type: String,
			badge: String,
			url: String
		},
		finances: {
			goal: {
				name: String,
				amount: Number,
				collected: Number
			},
			donationOptions: [Schema.Types.Mixed],
			customDonation: {
				paypal: Boolean,
				url: String,
				mollie: Boolean
			},
			sponsors: [
				{
					name: String,
					url: String
				}
			]
		},
		platforms: Schema.Types.Mixed,
		badges: [String],
		screenshots: [
			{
				src: String,
				alt: Schema.Types.Mixed
			}
		],
		alternativeTo: [
			{
				type: Schema.Types.ObjectId,
				ref: 'Alternative'
			}
		],
		features: [{ type: Schema.Types.ObjectId, ref: 'Feature' }],
		importHelper: Schema.Types.Mixed,
		externalData: {
			type: Schema.Types.ObjectId,
			ref: 'ExternalProjectData'
		}
	},
	{
		id: false,
		toJSON: { virtuals: true },
		timestamps: true
	}
)

const importRepoDataSchema = new Schema(
	{
		date: {
			type: Schema.Types.Date,
			default: new Date()
		},
		repos: Schema.Types.Mixed
	},
	{ _id: false }
)

const externalProjectDataSchema = new Schema(
	{
		projectId: {
			type: Schema.Types.ObjectId,
			ref: 'Project'
		},
		lastRepoImport: importRepoDataSchema,
		history: [importRepoDataSchema]
	},
	{
		id: false,
		toJSON: { virtuals: true },
		collection: 'external_projects_data'
	}
)

export const ProjectModel = mongoose.model('Project', projectSchema)
export const ExternalProjectDataModel = mongoose.model('ExternalProjectData', externalProjectDataSchema)
